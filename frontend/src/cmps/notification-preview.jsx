import Fade from '@mui/material/Fade';
import { AppAvatar } from './general/app-avatar';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import MemberAddIcon from '@mui/icons-material/PersonAdd';
import InviteIcon from '@mui/icons-material/Markunread';
import { Link } from 'react-router-dom';
import { formatDistance } from 'date-fns';
import _ from 'lodash';
import { utilService } from '../services/util.service';
import { cardService } from '../services/board-services/card.service';

export function NotificationPreview({ notification, user, onToggleMarkAsRead, toggleMenu, currentUser }) {







    console.log(notification.values);



    const ToolTip = styled(({ className, ...props }) => <Tooltip {...props} classes={{ popper: className }} />)(
        ({ theme }) => ({
            [`& .${tooltipClasses.tooltip}`]: {
                backgroundColor: 'rgb(23, 43, 77)',
                color: 'rgb(224, 226, 231)',
                padding: '4px 6px',
                fontSize: '0.7rem',
            },
        })
    );
    const CardLink = () => <Link to={notification.url} className="link">{notification.card.title}</Link>;

    // console.log(notification.user.fullname);

    const NotificationView = () => {

        switch (notification.type) {
            case 'ADD-CARD':
                return <span>added <CardLink /> to {notification.values.listTitle}</span>;
            case 'UPDATE-TITLE':
                return <span>Updated title on card <CardLink /></span>;
            case 'ADD-MEMBER':
                if (notification.values.member._id === user._id) return <span>joined to <CardLink /></span>;
                return <span>added <span className="created-by" style={{ margin: 0 }}>{notification.values.member._id === currentUser ? 'you' : notification.values.member.fullname || notification.values.member.username}</span> to <CardLink /></span>;
            case 'REMOVE-MEMBER':
                if (notification.values.member._id === user._id) return <span>left <CardLink /></span>;
                return <span>removed <span className="created-by" style={{ margin: 0 }}>{notification.values.member.fullname || notification.values.member.username}</span> from <CardLink /></span>;
            case 'ADD-CHECKLIST':
                return <span>added checklist "{notification.values.title}" to <CardLink /></span>;
            case 'CHECKLIST-COMPLETE':
                return <span>completed "{notification.values.title}" on <CardLink /></span>;
            /* case 'ADD-LOCATION':
              const { lat, lng } = notification.values.location;
              return <div>
                <div>added location {notification.values.title} to <CardLink /></div>
                <img src={`https://maps.googleapis.com/maps/api/staticmap?key=${ key }&libraries=places&zoom=13&size=225x66&markers=icon%3Ahttps://trello.com/images/location-marker.png%7C${ lat },${ lng }`}
                  alt='card-location' />
              </div>; */
            //   'REMOVE-ATTACHMENT'
            case 'REMOVE-ATTACHMENT':
                return <span>removed an attachment from <CardLink /></span>;
            case 'UPDATE-ATTACHMENT':
                return <span>updated an attachment on <CardLink /></span>;

            // UPDATE-ATTACHMENT
            case 'ADD-ATTACHMENT':
                const isValidImg = utilService.isValidImg(notification.values.attachment.url);
                const AttachmentName = _.truncate(notification.values.attachment.name, { length: 20 }) || _.truncate(notification.values.attachment.url, { length: 20 });
                return <div>

                    <div>attached {' '}
                        {isValidImg && <a className="link" target="_blank" rel="noreferrer" download href={notification.values.attachment.url}>{AttachmentName} </a>}
                        {!isValidImg && <span> {AttachmentName} </span>}
                        to <CardLink /></div>

                    {isValidImg && <img src={notification.values.attachment.url} alt='attachment' style={{ width: '50px', height: '50px' }} />}
                </div>;
            case 'ADD-DUE-DATE':
                return <span>set the due date on <CardLink /> to {' '}
                    <span className={`due-date ${cardService.checkDueDate({ date: notification.values.date, isComplete: false })}`}>
                        <span className="due-date-icon"></span>
                        <span>{utilService.getFormattedDate(notification.values.date, true)}</span>
                    </span>
                </span>;
            case 'MARK-DUE-DATE':
                return <span>marked the due date on <CardLink /> {notification.values.dueDate.isComplete ? 'complete' : 'incomplete'} </span>;
            // SET-COLOR-COVER
            case 'ADD-COVER':
                return <><div>set cover image on <CardLink /></div> <img src={notification.values.cover.url} alt='cover' style={{ maxHeight: '100px' }} /></>;
            case 'SET-COLOR-COVER':
                return <div>set <span className={'label ' + notification.values.color}> </span> as a cover color  on <CardLink /></div>;

            case 'REMOVE-COVER':
                return <span>removed cover image on <CardLink /></span>;
            case 'ADD-LABEL':
                return <span>added label <span className={'label ' + notification.values.label.color}> </span> on <CardLink /></span>;
            case 'REMOVE-LABEL':
                return <span>removed label <span className={'label ' + notification.values.label.color}></span> from <CardLink /></span>;
            case 'ADD-COMMENT':
                // console.log(currentUser);
                return <span> {(notification.values.mentionedUsersIds && notification.values.mentionedUsersIds.find(mentionedId => mentionedId === currentUser)) ? 'mentioned you in a comment on ' : 'commented on'} <CardLink /></span>;
            case 'DELETE-COMMENT':
                // console.log(currentUser);
                return <span> deleted his comment on <CardLink /></span>;
            case 'UPDATE-COMMENT':
                return <span>updated his comment on <CardLink /></span>;


            case 'CHANGE-DESCRIPTION':
                return <span>changed the discription of <CardLink /></span>;
            // 'ADD-ITEM-TO-CHECK-LIST'  // 
            case 'ADD-ITEM-TO-CHECK-LIST':
                return <span>{`added an item to check list ${notification.values.title} on `} <CardLink /></span>;
            case 'DELETE-CHECK-LIST':
                return <span>{`deleted checklist ${notification.values.title} on `} <CardLink /></span>;
            // DELETE-CHECK-LIST
            case 'REMOVE-ITEM-FROM-CHECK-LIST':
                return <span>{`removed item ${notification.values.removedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
            // DELETE-CHECK-LIST
            case 'UPDATE-ITEM-FROM-CHECK-LIST':
                return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;

            case 'UPDATE-CHECK-LIST':
                // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
                return <span>{`updated checklist ${notification.values.title} on `} <CardLink /></span>;

            case 'ARCHIVE-CARD':
                // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
                return <span>moved <CardLink />  to archive </span>;
            case 'UNARCHIVE-CARD':
                // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
                return <span>unarchived <CardLink /></span>;

            case 'REMOVE-CARD':
                // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
                return <span>{`removed ${notification.card.title}`}   </span>;

            case 'MOVE-CARD':
                // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
                // return <span>{`moved ${notification.card.title}` to}   </span>;
                return <span>{`moved `}  <CardLink /> {` to list ${notification.values.listTitle}`}   </span>;

            case 'COPY-CARD':
                // return <span>{`changed item ${notification.values.updatedItemtitle}  of checklist ${notification.values.checkListTitle} on `} <CardLink /></span>;
                return <span>{`copied ${notification.card.title}  in list ${notification.values.listTitle} `}   </span>;


            //COPY-CARD
            // REMOVE-CARD


            // DELETE-CHECK-LIST
            // this.props.updateField({ checklists },'UPDATE-ITEM-FROM-CHECK-LIST',{checkListTitle:affectedChecklist.title,updatedItemtitle:updatedItem1.title});


            // UPDATE-ITEM-FROM-CHECK-LIST
            // REMOVE-ITEM-FROM-CHECK-LIST',{checkListTitle:affectedChecklist.title,removedItemtitle:removedItem.title}



            default:
                return <span></span>;
        }




        // );



    }


    return (
        <div className="notification" style={{ backgroundColor: notification.isRead ? '#fff' : '#e4f7fa' }}>
            <div className="notification-inner">
                <div className="notification-read-btn">
                    {/* this btn have a before with icon */}
                    <ToolTip
                        title={notification.isRead ? 'Mark unread' : 'Mark read'}
                        TransitionComponent={Fade}
                        TransitionProps={{ timeout: 300 }}
                        placement="bottom">
                        <button className={notification.isRead ? 'read' : 'unread'} onClick={() => onToggleMarkAsRead(notification.id)}></button>
                    </ToolTip>
                </div>
                <div className="notification-inner-header">
                    <span>{notification.title}</span>
                </div>
                <div className="notification-inner-user">
                    <div className="user-avatar">
                        <AppAvatar member={user} />
                    </div>
                    <div className="user-name">
                        <span>{notification.user.fullname}</span>
                    </div>
                </div>
                <div className="notification-inner-content">
                    {notification.type === 'invite' &&
                        <div className="icon">
                            <span>
                                <InviteIcon />
                            </span>
                        </div>

                    }
                    <div className="description">
                        {notification.type === 'invite' ?
                            <>
                                <p>{notification.txt}, <Link to={notification.url}>Join now!</Link></p>

                                <span className="date">{formatDistance(notification.sentAt, Date.now(), { addSuffix: true })}</span>
                            </> :
                            <>

                                {/* <section className="side-menu-activities"> */}

                                {/* <div className="activity"> */}

                                <NotificationView />
                                {/* </div> */}
                                <br></br>
                                <span className="date">{formatDistance(notification.sentAt, Date.now(), { addSuffix: true })}</span>
                                {/* </section> */}
                                {/* <span className="created-at">
                                    {formatDistance(notification.sentdAt, Date.now(), { addSuffix: true })}
                                </span> */}
                                {/* 
                                <p>{notification.txt} <Link onClick={() => toggleMenu(null)} to={notification.url}>{notification.cardTitle}</Link></p>
                                <span className="date">{formatDistance(notification.sentAt, Date.now(), { addSuffix: true })}</span> */}
                            </>
                        }
                    </div>
                </div>
            </div>
        </div >
    )
}








