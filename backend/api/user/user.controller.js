const userService = require('./user.service');
const socketService = require('../../services/socket.service');
const logger = require('../../services/logger.service');
const asyncLocalStorage = require('../../services/als.service');
const webpush = require('web-push');
var nodemailer = require('nodemailer');



async function getUser(req, res) {
  try {

    const user = await userService.getById(req.params.id);
    res.send(user);
  } catch (err) {
    logger.error('Failed to getddddddd user', err);
    res.status(500).send({ err: 'Failed to get user' });
  }
}





async function sendMailToUser(req, res) {
  // try {


  const transporter = nodemailer.createTransport({
    port: 465,               // true for 465, false for other ports
    host: "smtp.gmail.com",
    auth: {
      user: 'powerfulmailer@gmail.com',
      pass: 'powerful2022',
    },
    secure: true,
  });


console.log("req.body ==============>",req.body);

// res.send(req.body);


  transporter.sendMail(req.body, function (err, info) {
    if (err) {
      console.log("************", err)

      res.send(err);

    }
    else
    console.log(info);

    res.send(info);

  });

  // const user = await userService.getById(req.params.id);
  // } catch (err) {
  //   logger.error('Failed to get user', err);
  //   res.status(500).send({ err: 'Failed to get user' });
  // }
}



async function getUsers(req, res) {
  try {

    let filterBy

    let users

    if (req.query.ids) {
      // filterBy = {
      //   name: req.query.ids
      // };
      users = await userService.query(req.query.ids, true);

    } else {
      filterBy = {
        name: req.query?.name || '',
      };

      users = await userService.query(filterBy);

    }

    // if(req.query.name)


    res.send(users);
  } catch (err) {
    logger.error('Failed to get users', err);
    res.status(500).send({ err: 'Failed tdsvsdvsdvso get users' });
  }
}

async function updateUser(req, res) {
  try {
    const user = req.body;
    const savedUser = await userService.update(user);
    const alsStore = asyncLocalStorage.getStore();
    const userId = alsStore.userId;
    if (userId !== savedUser._id.toString()) {
      logger.debug('server sending push event');
      webpush
        .sendNotification(
          savedUser.subscription,
          JSON.stringify({
            title: 'Swello - New Activity',
            text: savedUser.notifications[0].txt,
            url: savedUser.notifications[0].url,
            image: 'https://logos-world.net/wp-content/uploads/2021/02/Trello-Emblem.png',
          })
        )
        .catch(err => logger.error('err in sendNotification:', err));
      socketService.emitToUser({
        type: socketService.SOCKET_EVENT_USER_UPDATED,
        data: savedUser,
        userId: savedUser._id,
      });
    }
    res.send(savedUser);
  } catch (err) {
    logger.error('Failed to update user', err);
    res.status(500).send({ err: 'Failed to update user' });
  }
}

module.exports = {
  getUser,
  getUsers,
  updateUser,
  sendMailToUser
};
